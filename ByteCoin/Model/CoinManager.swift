import Foundation

protocol CoinManagerDelegate {
    func didCompleteCoin(with lastPrice: CoinData)
    func didFailWithError(error: Error)
}

struct CoinManager {
    
    let baseURL = "https://rest.coinapi.io/v1/exchangerate/BTC"
    let apiKey = "847ECD21-852B-49F2-A5AE-84B45BDF2990"
    let currencyArray = ["AUD", "BRL","CAD","CNY","EUR","GBP","HKD",
                         "IDR","ILS","INR","JPY","MXN","NOK","NZD",
                         "PLN","RON","RUB","SEK","SGD","USD","ZAR"]
    
    var delegate: CoinManagerDelegate?
    
    func getCoinPrice(for currency: String) {
        let urlString = baseURL + "/\(currency)?apikey=\(apiKey)"
        performRequest(with: urlString)
    }
    
    private func performRequest(with url: String) {
        guard let url = URL(string: url) else { return }
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: url) { data, response, error in
            let decoder = JSONDecoder()
            do {
                guard let safeData = data else { return }
                let decodedData = try decoder.decode(CoinData.self, from: safeData)
                delegate?.didCompleteCoin(with: decodedData)
            } catch {
                delegate?.didFailWithError(error: error)
            }
        }.resume()
    }
}
