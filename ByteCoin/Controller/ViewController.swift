import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var bitcoinLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var currencyPicker: UIPickerView!
    
    var coinManager = CoinManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currencyPicker.delegate = self
        currencyPicker.dataSource = self
        coinManager.delegate = self
        
        let currentRow = currencyPicker.selectedRow(inComponent: 0)
        currencyLabel.text = coinManager.currencyArray[currentRow]
    }
}

// MARK: - UIPickerView
extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return coinManager.currencyArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return coinManager.currencyArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let currencyText = coinManager.currencyArray[row]
        coinManager.getCoinPrice(for: currencyText)
    }
}

// MARK: - CoinManagerDelegate
extension ViewController: CoinManagerDelegate {
    func didCompleteCoin(with lastPrice: CoinData) {
        DispatchQueue.main.async { [self] in
            bitcoinLabel.text = String(format: "%.2f", lastPrice.rate)
            currencyLabel.text = lastPrice.asset_id_quote
        }
    }
    
    func didFailWithError(error: Error) {
        print(error.localizedDescription)
    }
}
